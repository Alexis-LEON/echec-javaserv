/**
 * 
 */
package pieces;

import data.Couleur;


public interface iPiece {

    /**
     * Obtient la couleur de la pièce
     * @return Retourne une valeur de l'énumaration CouleurPiece.
     */
    public Couleur getCouleur();

    /**
     * Obtient le nom de la pièce. La casse dépend de la couleur du pion.
     * @return Retourne dans un char la lettre associée à la pièce.
     */
    public abstract char getNom();

    /**
     * Obtient le nom complet de la pièce.
     * @return Retourne un String contenant le nom complet de la pièce.
     */
    public abstract String getNomComplet();
}
